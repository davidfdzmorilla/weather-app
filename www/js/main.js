'use strict'



// Boton e inputText . Busca el tiempo introduciendo el nombre de ciudad y opcional codigo pais.

const searchButton = document.querySelector('#submit')

const search = () => {
    let city = document.querySelector('#search').value
    const getWeather = async () => {
        try {
            const API_KEY = '89618dc70daeb697f6acda169ca6f992'
            const weatherUrl = `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${API_KEY}&units=metric&lang=sp`
            const response = await fetch(weatherUrl)
            const weatherData = await response.json()
            const temp = Math.round(weatherData.main.temp)
            const humidity = weatherData.main.humidity
            const description = weatherData.weather[0].description
            //  Condicionales para mostrar imagen icono tiempo
            let weatherIcon = ''
            if (weatherData.weather[0].id == 800) {
                weatherIcon = '/images/sun.svg'
            } else if (weatherData.weather[0].id == 801) {
                weatherIcon = '/images/sun-cloudy-white.svg'
            } else if (weatherData.weather[0].id > 700 && weatherData.weather[0].id < 800) {
                weatherIcon = '/images/fog.svg'
            } else if (weatherData.weather[0].id == 802) {
                weatherIcon = '/images/cloud-white.svg'
            } else if (weatherData.weather[0].id == 803 || weatherData.weather[0].id == 804) {
                weatherIcon = '/images/nube.svg'
            } else if (weatherData.weather[0].main == 'Snow') {
                weatherIcon = '/images/snow.svg'
            } else if (weatherData.weather[0].main == 'Rain' || weatherData.weather[0].main == 'Drizzle') {
                weatherIcon = '/images/rain.svg'
            } else if (weatherData.weather[0].main == 'Thunderstorm') {
                weatherIcon = '/images/storm.svg'
            }
            //  Creo card con informacion
            container.innerHTML = `
            <article id="card">
            <header id="header-card"><p>${weatherData.name}, ${weatherData.sys.country}.</p></header>
            <h1 id="grados">${temp}ºC</h1>
            <h2 id="humidity">Humedad: ${humidity}%</h2>
            <img src="${weatherIcon}" id="weather-icon"/>
            <footer id="footer-card">${description}.</footer>
            </article>
            `
            //  Creom info error
        } catch (error)  {
            container.innerHTML = `
            <article id="card">
            <p id="mensajeError"> Lo sentimos, no hemos encontrado esta ubicación en nuestra base de datos. ❌</p>
            </article>
            `
        }
    }
    document.querySelector('#search').value = ''
    getWeather()
}

//  Funcion para que al presionar enter se envié la City
document.getElementById('search')
    .addEventListener('keyup', function(e) {
        e.preventDefault()
        if (e.keyCode === 13) {
            document.getElementById('submit').click()
        }
    })

searchButton.addEventListener('click', search)



// Funcion que me regresa la posición con el boton position y el tiempo.

const geoButton = document.querySelector('#gps-search')
const container = document.querySelector('#container')

const success = async pos => {
    const latitude = pos.coords.latitude
    const longitude = pos.coords.longitude
    const getWeather = async () => {
        try {
            const API_KEY = '89618dc70daeb697f6acda169ca6f992'
            const weatherUrl = `https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=${API_KEY}&units=metric&lang=sp`
            const response = await fetch(weatherUrl)
            const weatherData = await response.json()
            const temp = Math.round(weatherData.main.temp)
            const humidity = weatherData.main.humidity
            const description = weatherData.weather[0].description
            document.querySelector('#search').value = ''
            //  Condicionales para mostrar imagen icono tiempo
            let weatherIcon = ''
            if (weatherData.weather[0].id == 800) {
                weatherIcon = '/images/sun.svg'
            } else if (weatherData.weather[0].id == 801) {
                weatherIcon = '/images/sun-cloudy-white.svg'
            } else if (weatherData.weather[0].id > 700 && weatherData.weather[0].id < 800) {
                weatherIcon = '/images/fog.svg'
            } else if (weatherData.weather[0].id == 802) {
                weatherIcon = '/images/cloud-white.svg'
            } else if (weatherData.weather[0].id == 803 || weatherData.weather[0].id == 804) {
                weatherIcon = '/images/nube.svg'
            } else if (weatherData.weather[0].main == 'Snow') {
                weatherIcon = '/images/snow.svg'
            } else if (weatherData.weather[0].main == 'Rain' || weatherData.weather[0].main == 'Drizzle') {
                weatherIcon = '/images/rain.svg'
            } else if (weatherData.weather[0].main == 'Thunderstorm') {
                weatherIcon = '/images/storm.svg'
            }
            //  Condicionales para mostrar imagen icono tiempo
            container.innerHTML = `
            <article id="card">
            <header id="header-card"><p>${weatherData.name}, ${weatherData.sys.country}.</p></header>
            <section id="grdosContainer">
            <h1 id="grados">${temp}ºC</h1></section>
            <h2 id="humidity">Humedad: ${humidity}%</h2>
            <img src="${weatherIcon}" id="weather-icon"/>
            <footer id="footer-card">${description}.</footer>
        </article>
        `
        //  Creom info error
        } catch (error)  {
            container.innerHTML = `
            <article id="card">
            <p id="mensajeError"> Lo sentimos, no hemos encontrado esta ubicación en nuestra base de datos. ❌</p>
            </article>
            `
        }
    }
    getWeather()
}
const geo = navigator.geolocation
const getPosition = async () => {
    try {
        const pos = await new Promise((resolve, reject) => {
            return geo.getCurrentPosition(resolve, reject)
        })
        success(pos)
    } catch (error) {
        console.error(error)
    }
}




geoButton.addEventListener('click', getPosition)

// Funcion que me devuelve el tiempo






